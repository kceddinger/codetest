import TriviaCard from './TriviaCard';
import React from 'react';

const TrivaCards = (props) => {

  return (
    <div>
      {props.cards.map((card, i)=> {
        return <TriviaCard
        details={card}
        handleDelete={props.handleDelete}
        key={i}/>
      })}
    </div>
  )
}

export default TrivaCards;