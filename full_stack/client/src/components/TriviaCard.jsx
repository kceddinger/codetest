import SwipeableCard from 'react-tinder-card'
import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles({
  root: {
    position: "relative",
    width: "35vw",
    height: "45vw",
    borderRadius: "10px",
    backgroundSize: "cover",
    backgroundPosition: "center",
    marginTop: "5vh",
  },
  cardContainer: {
    display: "flex",
    justifyContent: "center"
  },
  media: {
    height: "20vw",
  },
  swipe: {
    position: "absolute",
    overflow: "hidden"
  },
});

const TriviaCard = (props) => {
  let {url, description, fact, id} = props.details;

  const classes = useStyles();

  return (
    <div className={classes.cardContainer}>
    <SwipeableCard  className={classes.swipe}>
      <Card className={classes.root}>
        <CardMedia
        className={classes.media}
        component="img"
        image={url}
        title="Starfleet Member"
        alt="Starfleet Member"
        />
        <CardContent>
          <Typography gutterBottom variant="subtitle2" component="h2">{description}</Typography>
          <Typography variant="body2" color="textSecondary" component="p">{fact}</Typography>
        </CardContent>
        <CardActions>
        <Button size="small" color="primary" onClick={()=>props.handleDelete(id)}>
          Delete
        </Button>
        </CardActions>
      </Card>
    </SwipeableCard>
    </div>

  )
}
export default TriviaCard;
