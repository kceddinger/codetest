import React from "react";
import axios from "axios";
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TriviaCards from './TriviaCards.jsx';
import Form from './Form';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [],
      newUrl: '',
      newDesc: '',
      newFact: '',
      toggleFormOn: false
    };
    this.getData = this.getData.bind(this);
    this.toggleForm = this.toggleForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    axios
      .get("/cards/all")
      .then(({data}) => {
        this.setState({
          cards: [...data]
        });
      })
      .catch((err) => {
        console.error(err);
      })
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  toggleForm() {
    this.setState({
      toggleFormOn: !this.state.toggleFormOn
    })
  }

  handleDelete(id){
    axios
    .delete(`/cards/${id}`)
    .then(()=> this.getData())
    .catch(err => console.log(err))
  }

  handleSubmit(e){
    e.preventDefault();
    axios
    .post('/cards/add', { url: this.state.newUrl, description: this.state.newDesc, fact: this.state.newFact})
    .then(() => {
      this.getData();
      this.toggleForm();
    })
    .catch(err => console.error(err))
  }

  render() {
    return (
      <CssBaseline>
        <div className="main">
          <div className="header">
            <h2>Welcome to the Bridge</h2>
            <h3>Fun Facts about Star Trek: The Next Generation!</h3>
          </div>
            <Button size="small" color="primary" onClick={this.toggleForm}>
              ADD NEW CARD
            </Button>
          {this.state.toggleFormOn ?
            <Form
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
            />
          : <TriviaCards
              cards={this.state.cards}
              handleDelete={this.handleDelete}
            />}
          <div className="options">
          </div>
        </div>
      </CssBaseline>
    );
  }
}

export default App;