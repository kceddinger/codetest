import React from 'react';
import { FormControl, TextField} from '@material-ui/core';
import Button from '@material-ui/core/Button';

const Form = (props) => {
  return (
    <form action="submit">
      <FormControl margin="normal">
        <TextField
          onChange={props.handleChange}
          variant="outlined"
          label="Link to Picture"
          name="newUrl"
        ></TextField>
        <TextField
          onChange={props.handleChange}
          variant="outlined"
          placeholder="write a description of the card"
          name="newDesc"
          label="Description"
          multiline
          rows={3}
          rowsMax={4}
        ></TextField>
        <TextField
          onChange={props.handleChange}
          name="newFact"
          variant="outlined"
          placeholder="write a fun fact"
          label="Fun Fact"
          multiline
          rows={3}
          rowsMax={4}
        ></TextField>
        <Button size="small" color="primary" onClick={props.handleSubmit}>
          Submit
        </Button>
      </FormControl>
    </form>
  )
}

export default Form;