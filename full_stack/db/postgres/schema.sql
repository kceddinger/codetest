DROP DATABASE IF EXISTS deck;

CREATE DATABASE deck;

\c deck;

CREATE TABLE cards (
  id SERIAL NOT NULL PRIMARY KEY,
  "url" text,
  "description" text,
  "fact" text
)
