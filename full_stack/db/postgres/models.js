const db = require('./index.js');

const dbHelpers = {
  getAll: (callback) => {
    db.query('SELECT * FROM cards', (err, results) => {
      if (err) {
        callback(err);
      } else {
        callback(null, results);
      }
    })
  },
  getOne: (id, callback) => {
    db.query(`SELECT * FROM cards WHERE id=${id};`, (err, results) => {
      if (err) {
        callback(err);
      } else {
        callback(null, results);
      }
    })
  },
  addOne: (data, callback) => {
    let {url, description, fact} = data;
    let queryStr = `INSERT INTO cards (url, description, fact) VALUES ('${url}', '${description}', '${fact}');`
    db.query(queryStr, (err, results) => {
      if (err) {
        callback(err);
      } else {
        callback(null, results);
      }
    })
  },
  updateOne: (data, callback) => {
    let {id, url, description, fact} = data;
    let queryStr = `UPDATE cards SET url='${url}', description='${description}', fact='${fact}' WHERE cards.id = ${id};`
    db.query(queryStr, (err, results) => {
      if (err) {
        callback(err);
      } else {
        callback(null, results);
      }
    })
  },
  deleteOne: (id, callback)=> {
    db.query(`DELETE FROM cards WHERE cards.id=${id};`, (err, results) => {
      if (err) {
        callback(err);
      } else {
        callback(null, results);
      }
    })
  },
  deleteAll: (callback)=> {
    db.query(`DELETE FROM cards;`, (err, results) => {
      if (err) {
        callback(err);
      } else {
        callback(null, results);
      }
    })
  }
}

module.exports = dbHelpers;