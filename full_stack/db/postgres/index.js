const Pool = require('pg').Pool
const auth = require('./auth.js')

const connection = new Pool({
  user: auth.user,
  host: 'localhost',
  database: 'deck',
  password: auth.pw,
  port: 5432,
})

module.exports = connection;