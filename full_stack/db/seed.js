const db = require('./postgres/index.js');

const startfleet = [
  {
    url: 'https://www.wixiban.com/images/tnginaugural/004.jpg',
    description: 'Captain Jean-Luc Picard (Sir Patrick Stewart) is the captain of the starship USS Enterprise.',
    fact: `At the suggestion of the producers, Sir Patrick Stewart wore a hairpiece for his first meeting with Paramount Pictures executives. Evidently, the creative staff on the show worried that Paramount would veto Stewart''s casting if they knew he was bald. After the meeting, the executives agreed to cast Stewart, on the condition that he not wear the same ridiculous toupee.`
  },
  {
    url: 'https://www.wixiban.com/images/tnginaugural/005.jpg',
    description: 'Commander William T. Riker (Jonathan Frakes) is the first officer of the USS Enterprise.',
    fact: 'Riker has a distinct approach to sitting on chairs. He is often seen lifting his leg oven the back of a chair to sit instead of walking around to the front of it. One likely explaination for this is the back injury Jonathan Frakes suffered while working at a job moving furniture.'
  },
  {
    url: 'https://www.wixiban.com/images/tnginaugural/006.jpg',
    description: 'Lieutenant Commander Data (Brent Spiner), an android, is the second officer of the USS Enterprise as well as the Operations Officer.',
    fact: 'As the show progressed, producers decided to give Data a cat named Spot. Ironically, Brent Spiner despises cats, and admitted in interviews that with most cats, the feeling was mutual.'
  },
  {
    url: 'https://www.wixiban.com/images/tnginaugural/007.jpg',
    description: 'Lieutenant Worf (Michael Dorn), a Klingon, is the Security Officer of the USS Enterprise.',
    fact: `Michael Dorn''s voice got naturally deeper as a result of the vocal inflections he used for the part of Worf.`
  },
  {
    url: 'https://www.wixiban.com/images/tnginaugural/008.jpg',
    description: 'Lt. Commander Geordi LaForge (LeVar Burton) is the Chief Science Officer of the USS Enterprise.',
    fact: `Geordi''s VISOR was improvised on the first day of shooting using chiefly an automotive air filter and a hair band.`
  },
  {
    url: 'https://www.wixiban.com/images/tnginaugural/009.jpg',
    description: 'Counselor Deanna Troi (Marina Sirtis) is the Counselor of the USS Enterprise.',
    fact: `Denise Crosby was originally cast to play Counselor Troi, and Marina Sirtis was cast as a security chief named Lieutenant Macha Hernandez. Shortly before filming the pilot, the two switched roles, and the security chief''s name was changed to Lieutenant Natasha Yar.`
  },
  {
    url: 'https://www.wixiban.com/images/tnginaugural/010.jpg',
    description: 'Dr. Beverly Crusher (Gates McFadden) is the Chief Medical Officer of the USS Enterprise.',
    fact: 'Gates McFadden was pregnant during most of the fourth season. Since it was not incorporated into the series, directors used a variety of techniques to hide her pregnancy on-screen.'
  },
  {
    url: 'https://www.wixiban.com/images/tnginaugural/011.jpg',
    description: 'Guinan (Whoopi Goldberg) is the bartender in the Ten-Foward Lounge of the USS Enterprise.',
    fact: 'Gene Roddenberry had not realized that until Star Trek: The Original Series (1966) premiered, there were no African-American characters in science fiction movies and television. When he had lunch with Whoopi Goldberg, she inquired about wanting to appear on this show, Roddenberry created the role of Guinan for her.'
  },
  {
    url: 'https://www.wixiban.com/images/tnginaugural/012.jpg',
    description: 'Wesley Crusher (Wil Wheaton) is an Ensign aboard the USS Enterprise',
    fact: `When the cast decided to lobby for a salary increase, Wil Wheaton''s first offer from the producers was to instead have his character''s rank raised to Lieutenant. His response was, "So what should I tell my landlord when I can''t pay my rent? "Don''t worry, I just made Lieutenant?"`
  }
]


const seedDatabase = (items) => {

  items.forEach((item) => {
    db.query(`INSERT INTO cards (url, description, fact) VALUES ('${item.url}','${item.description}', '${item.fact}');`, (err, results) => {
      if (err) {
        console.error(`error seeding:${err}`);
      } else {
        console.log('success seeding items')
      }
    })
  })
  db.end();
}

seedDatabase(startfleet);
