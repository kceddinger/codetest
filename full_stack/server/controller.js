const dbHelpers = require('../db/postgres/models.js');

const controller = {
  getAll: (req, res) => {
    dbHelpers.getAll((err, {rows}) => {
      if (err) {
        res.status(400).send('Cannot Get All Cards')
      } else {
        res.status(200).json(rows)
      }
    })
  },
  getOne: (req, res) => {
    dbHelpers.getOne(req.params.id, (err, {rows}) => {
      if (err) {
        res.status(400).send('Cannot Get Card')
      } else {
        res.status(200).json(rows)
      }
    })
  },
  getOne: (req, res) => {
    dbHelpers.getOne(req.params.id, (err, {rows}) => {
      if (err) {
        res.status(400).send('Cannot Get Card')
      } else {
        res.status(200).json(rows)
      }
    })
  },
  updateOne: (req, res) => {
    dbHelpers.updateOne(req.body, (err) => {
      if (err) {
        res.status(400).send('Cannot update card')
      } else {
        res.status(200).send('Sucessfully updated a card!')
      }
    })
  },
  addOne: (req, res) => {
    dbHelpers.addOne(req.body, (err) => {
      if (err) {
        res.status(400).send('Cannot add card')
      } else {
        res.status(200).send('Sucessfully added a card!')
      }
    })
  },
  deleteOne: (req, res) => {
    dbHelpers.deleteOne(req.params.id, (err, result) => {
      if (err) {
        res.status(400).send('Cannot delete card')
      } else {
        res.status(200).send('Successfully deleted a card!')
      }
    })
  },
  deleteAll: (req, res) => {
    dbHelpers.deleteAll((err, result) => {
      if (err) {
        res.status(400).send('Cannot delete cards')
      } else {
        res.status(200).send('Successfully deleted all cards!')
      }
    })
  },
};

module.exports = controller;