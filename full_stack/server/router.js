const router = require('express').Router();
const controller = require('./controller.js');

router
.route('/all')
.get(controller.getAll)
.delete(controller.deleteAll)

router
.route('/add')
.post(controller.addOne)

router
.route('/update')
.patch(controller.updateOne)

router
.route('/:id')
.get(controller.getOne)
.delete(controller.deleteOne)

module.exports = router;